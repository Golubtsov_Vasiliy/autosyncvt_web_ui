#How install and deploy

##install curl:

	sudo apt install curl 

##install latest node and npm: 

	curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
	sudo apt-get install nodejs

##angular work enviroment:

npm install -g @angular/cli



##deploy project:

	git clone https://bitbucket.org/Golubtsov_Vasiliy/autosyncvt_web_ui.git
	cd projectDir
	npm install 

##run-server:

	cd projectDir
	ng serve --open

##More https://angular.io/guide/quickstart
